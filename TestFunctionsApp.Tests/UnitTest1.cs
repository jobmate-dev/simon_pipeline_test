using System;
using Xunit;

namespace TestFunctionsApp.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal(5, 3 + 2);
        }

        [Theory]
        [InlineData(5, 3, 2)]
        public void CalculateSum(int expected, int a, int b)
        {
            Assert.Equal(expected, a + b);
        }

    }
}
